package com.shmmy.examresults.parser;

import android.os.AsyncTask;
import android.util.Log;

import com.parser.phpbb.ForumThread;
import com.parser.phpbb.PhpBB3Parser;
import com.shmmy.examresults.UpdateOperationsCallback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by tilemachos on 7/8/15.
 */
public class PostsParser extends AsyncTask<ForumThread, Void, List<ForumThread>> {

    private static final String TAG = PostsParser.class.getSimpleName();

    private final PhpBB3Parser parser = new PhpBB3Parser();
    private final UpdateOperationsCallback mCallback;

    public PostsParser(UpdateOperationsCallback callback){
        mCallback = callback;
    }

    protected List<ForumThread> doInBackground(ForumThread... fts) {
        Log.d(TAG, "Start doInBackground");
        List<ForumThread> forumThreads = new ArrayList<>();
        Collections.addAll(forumThreads, fts);
        try {
            parser.parsePosts(forumThreads);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        Log.d(TAG, "End doInBackground");
        return forumThreads;
    }

    protected void onPostExecute(List<ForumThread> forumThreads) {
        mCallback.onItemsLoadComplete(forumThreads);
    }
}
