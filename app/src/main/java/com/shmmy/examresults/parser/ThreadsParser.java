package com.shmmy.examresults.parser;

import android.os.AsyncTask;
import android.util.Log;

import com.parser.phpbb.ForumThread;
import com.parser.phpbb.PhpBB3Parser;
import com.shmmy.examresults.UpdateOperationsCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tilemachos on 7/5/15.
 */
public class ThreadsParser extends AsyncTask<String, Void, List<ForumThread>> {

    private static final String TAG = ThreadsParser.class.getSimpleName();

    private final PhpBB3Parser parser = new PhpBB3Parser();
    private final UpdateOperationsCallback mCallback;

    public ThreadsParser(UpdateOperationsCallback callback){
        this.mCallback = callback;
    }

    protected List<ForumThread> doInBackground(String... url) {

        Log.d(TAG, "Start doInBackground");
        List<ForumThread> threads = new ArrayList<>();
        try {
            threads.addAll(parser.parseForumThreads(url[0]));
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        Log.d(TAG, "End doInBackground");
        return threads;
    }

    protected void onPostExecute(List<ForumThread> threads) {
        mCallback.onItemsLoadComplete(threads);
    }
}