package com.shmmy.examresults;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by tilemachos on 7/13/15.
 */
public class ScrollingSwipeRefreshLayoutBehavior extends PatchedScrollingViewBehavior {

    public ScrollingSwipeRefreshLayoutBehavior(Context context, AttributeSet attr){
        super(context, attr);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return super.layoutDependsOn(parent, child, dependency) || (dependency instanceof AppBarLayout);
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        boolean returnValue = super.onDependentViewChanged(parent, child, dependency);
        if (dependency instanceof AppBarLayout) {
            child.setEnabled(dependency.getY()==0);
        }
        return returnValue;
    }
}