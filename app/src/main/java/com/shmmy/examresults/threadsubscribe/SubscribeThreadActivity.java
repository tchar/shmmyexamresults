package com.shmmy.examresults.threadsubscribe;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.support.design.widget.FloatingActionButton;

import com.parser.phpbb.ForumThread;
import com.shmmy.examresults.R;
import com.shmmy.examresults.file.FileWriter;
import com.shmmy.examresults.utilities.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class SubscribeThreadActivity extends AppCompatActivity {

    private final static String TAG = SubscribeThreadActivity.class.getSimpleName();
    private final static String ARGS_THREADS_TO_SUBSCRIBE = TAG + ".threads.subscribable";
    private final static String ARGS_SUBSCRIBED_THREADS_URLS = TAG + ".threads.subscribed";
    private ThreadsController mThreadsController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribe_thread);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_activity_subscribe_thread);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);

        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.threadsRecyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(llm);
        ThreadsRVAdapter threadsRVAdapter = new ThreadsRVAdapter();
        recyclerView.setAdapter(threadsRVAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new FABListener());

        SwipeRefreshLayout srl = (SwipeRefreshLayout) findViewById(R.id.threadsRefreshLayout);
        srl.setColorSchemeResources(R.color.shmmy_blue);
        mThreadsController = new ThreadsController(this, srl, threadsRVAdapter);
        srl.setOnRefreshListener(mThreadsController);

        processSavedData(savedInstanceState);
    }

    private void processSavedData(Bundle bundle){

        // In case its clean run of this activity
        if (bundle == null){
            ArrayList<String> urls = getIntent()
                    .getStringArrayListExtra(Constants.THREADS_SUBSCRIBED_URLS_ARG);
            if (urls != null)
                mThreadsController.setSubscribedThreadsUrls(urls);
            mThreadsController.doRefresh();
        }
        // In case activity is recovering it's state
        else {
            ArrayList<String> urls = bundle.getStringArrayList(ARGS_SUBSCRIBED_THREADS_URLS);
            if (urls != null)
                mThreadsController.setSubscribedThreadsUrls(urls);

            Serializable s = bundle.getSerializable(ARGS_THREADS_TO_SUBSCRIBE);
            List<ForumThread> fts = (ArrayList<ForumThread>) s;
            if (fts != null){
                mThreadsController.setSubscribableThreads(fts);
            } else{
                mThreadsController.doRefresh();
            }
        }
    }

    public void exitWithResultOk(){
        this.setResult(RESULT_OK);
        this.finish();
    }


    @Override
    protected void onRestoreInstanceState(@NonNull Bundle inState) {
        super.onRestoreInstanceState(inState);
        processSavedData(inState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        ArrayList<String> urls = new ArrayList<>(mThreadsController.getSubscribedThreadsUrls());
        outState.putStringArrayList(ARGS_SUBSCRIBED_THREADS_URLS, urls);
        ArrayList<ForumThread> fts = new ArrayList<>(mThreadsController.getSubscribableThreads());
        outState.putSerializable(ARGS_THREADS_TO_SUBSCRIBE, fts);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class FABListener implements View.OnClickListener {

        private final Context mContext;

        public FABListener() {
            mContext = SubscribeThreadActivity.this;
        }

        @Override
        public void onClick(View v) {
            int selectedElements = mThreadsController.getSubscribedThreadsUrls().size();
            if (selectedElements > Constants.MAX_SELECTED_THREADS) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext)
                        .setCancelable(true)
                        .setTitle(getString(R.string.title_dialog_threads_subscribe))
                        .setMessage(getString(R.string.message_dialog_threads_subscribe))
                        .setPositiveButton(android.R.string.ok, null);
                    AlertDialog alert = builder.create();
                    alert.show();
                    return;
            }


            List<ForumThread> fts = new ArrayList<>();
            for (String s : mThreadsController.getSubscribedThreadsUrls()){
                String title = "";
                // This loop could be optimized, but max loop count is 4
                for (ForumThread ft : mThreadsController.getSubscribableThreads())
                    if (ft.getThreadSubUrl().equals(s)){
                        title = ft.getTitle();
                        break;
                    }
                ForumThread ft = new ForumThread(s);
                ft.setTitle(title);
                fts.add(ft);
            }

            ForumThread[] fta = new ForumThread[fts.size()];
            fta = fts.toArray(fta);
            new FileWriter(SubscribeThreadActivity.this, mThreadsController).execute(fta);
        }
    }
}
