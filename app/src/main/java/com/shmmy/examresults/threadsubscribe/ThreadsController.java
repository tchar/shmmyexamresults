package com.shmmy.examresults.threadsubscribe;

import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;

import com.parser.phpbb.ForumThread;
import com.shmmy.examresults.UpdateOperationsCallback;
import com.shmmy.examresults.AbstractController;
import com.shmmy.examresults.parser.ThreadsParser;
import com.shmmy.examresults.utilities.Constants;

import java.util.List;

/**
 * Created by tilemachos on 7/7/15.
 */
public class ThreadsController extends AbstractController
        implements SwipeRefreshLayout.OnRefreshListener, UpdateOperationsCallback{

    private static final String TAG = ThreadsController.class.getSimpleName();
    private static final int MODE_DO_REFRESH = 1;
    private final SwipeRefreshLayout mSwipeRefreshLayout;
    private final ThreadsRVAdapter mThreadsRVAdapter;
    private final SubscribeThreadActivity mActivity;

    public ThreadsController(SubscribeThreadActivity activity,
                             SwipeRefreshLayout swipeRefreshLayout,
                             ThreadsRVAdapter threadsRVAdapter){
        mActivity = activity;
        mSwipeRefreshLayout = swipeRefreshLayout;
        mThreadsRVAdapter = threadsRVAdapter;
    }

    private void setRefreshing(boolean bool){
        if (bool){
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
        }
        else{
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });
        }
    }

    @Override
    public void onRefresh() {
        setRefreshing(false);
    }

    @Override
    protected boolean canRefresh(int mode){
        return true;    // Can always refresh this
    }

    public void doRefresh(){
        doRefresh(MODE_DO_REFRESH);
    }

    @Override
    protected void doRefresh(int mode) {
        // Load items
        if (!canRefresh(mode))
            return;
        setRefreshing(true);
        new ThreadsParser(this).execute(Constants.ROOT_URL + Constants.RESULTS_URL);
    }

    @Override
    public void onItemsLoadComplete(List<ForumThread> fts) {
        // Update the adapter and notify data set changed
        // ...
        Log.d(TAG, "Start onItemsLoadComplete");
        mThreadsRVAdapter.setThreads(fts);
        mThreadsRVAdapter.notifyDataSetChanged();
        // Stop refresh animation
        setRefreshing(false);
        Log.d(TAG, "End onItemsLoadComplete");
    }

    @Override
    public void onFileReadComplete(List<ForumThread> fts) {
    }

    @Override
    public void onFileWriteComplete() {
        mActivity.exitWithResultOk();
    }

    public List<ForumThread> getSubscribableThreads(){
        return mThreadsRVAdapter.getForumThreads();
    }

    public void setSubscribableThreads(List<ForumThread> fts){
        onItemsLoadComplete(fts);
    }

    public List<String> getSubscribedThreadsUrls(){
        return mThreadsRVAdapter.getSubscribedThreadsUrls();
    }

    public void setSubscribedThreadsUrls(List<String> urls){
        mThreadsRVAdapter.setSubscribedThreadsUrls(urls);
    }
}

