package com.shmmy.examresults.threadsubscribe;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parser.phpbb.ForumThread;
import com.shmmy.examresults.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tilemachos on 7/6/15.
 */
public class ThreadsRVAdapter extends RecyclerView.Adapter<ThreadsRVAdapter.ThreadViewHolder> {

    private static final String TAG = ThreadsRVAdapter.class.getSimpleName();
    private final List<ForumThread> mThreads;
    private final List<String> mSubscribedThreadsUrls;

    public class ThreadViewHolder extends RecyclerView.ViewHolder implements SwitchCompat.OnClickListener{
        SwitchCompat switchView;
        View parentView;

        ThreadViewHolder(View itemView) {
            super(itemView);
            switchView = (SwitchCompat) itemView.findViewById(R.id.switchView);
            parentView = itemView;
            switchView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            String url = mThreads.get(getLayoutPosition()).getThreadSubUrl();
            if (mSubscribedThreadsUrls.contains(url))
                mSubscribedThreadsUrls.remove(url);
            else
                mSubscribedThreadsUrls.add(url);

            ThreadsRVAdapter.this.notifyItemChanged(getLayoutPosition());
        }
    }

    public ThreadsRVAdapter(){
        mThreads = new ArrayList<>();
        mSubscribedThreadsUrls = new ArrayList<>();
    }

    public List<ForumThread> getForumThreads(){
        return mThreads;
    }

    public void setThreads(List<ForumThread> threads) {
        mThreads.clear();
        mThreads.addAll(threads);
    }

    public ForumThread getItem(int position){
        return mThreads.get(position);
    }

    @Override
    public int getItemCount() {
        return mThreads.size();
    }

    @Override
    public ThreadViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.thread_recycler_view_element, viewGroup, false);
        return new ThreadViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ThreadViewHolder threadViewHolder, int i) {
        threadViewHolder.switchView.setText(mThreads.get(i).getTitle());
        // Set the selected state of the row depending on the position
        View rl = threadViewHolder.parentView.findViewById(R.id.threadsRelativeLayout);
        if (mSubscribedThreadsUrls.contains(mThreads.get(i).getThreadSubUrl())){
            threadViewHolder.switchView.setChecked(true);
        } else {
            threadViewHolder.switchView.setChecked(false);
        }
    }

    public void setSubscribedThreadsUrls(List<String> urls){
        mSubscribedThreadsUrls.clear();
        mSubscribedThreadsUrls.addAll(urls);
    }

    public List<String> getSubscribedThreadsUrls(){
        return mSubscribedThreadsUrls;
    }

}
