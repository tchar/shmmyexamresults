package com.shmmy.examresults;

import com.parser.phpbb.ForumThread;
import java.util.List;

/**
 * Created by tilemachos on 7/9/15.
 */
public interface UpdateOperationsCallback {

    void onItemsLoadComplete(List<ForumThread> fts);
    void onFileReadComplete(List<ForumThread> fts);
    void onFileWriteComplete();
}
