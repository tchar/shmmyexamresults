package com.shmmy.examresults.webview;

/**
 * Created by tilemachos on 26/10/2017.
 */

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.shmmy.examresults.R;

public class WebViewActivity extends Activity {

    public static final String URL_KEY = "url";
    private NonLeakingWebView webView;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        webView = (NonLeakingWebView) findViewById(R.id.webView1);
        webView.getSettings().setJavaScriptEnabled(true);
        String url = getIntent().getStringExtra(URL_KEY);
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                WebViewActivity.this.setTitle("Loading...");
                WebViewActivity.this.setProgress(progress * 100);

                if(progress == 100)
                    WebViewActivity.this.setTitle(R.string.title_activity_webview);
            }
        });
        webView.loadUrl(url);
    }

    @Override
    protected void onDestroy() {
        try {
            webView.destroy();
        }
        catch (Exception ex) {
            Log.e(WebViewActivity.class.getSimpleName() ,ex.getMessage());
        }
        super.onDestroy();
    }

}