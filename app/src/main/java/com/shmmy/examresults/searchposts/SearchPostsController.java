package com.shmmy.examresults.searchposts;

import android.content.Context;

import com.shmmy.examresults.AbstractPostsController;

/**
 * Created by tilemachos on 7/24/15.
 */
public class SearchPostsController extends AbstractPostsController {

    private SearchPostsRVAdapter mAdapter;

    public SearchPostsController(Context context, SearchPostsRVAdapter adapter){
        super(context);
        mAdapter = adapter;
    }

    @Override
    protected boolean canRefresh(int mode) {
        return true;
    }

    @Override
    protected void doRefresh(int mode) {
        // Do nothing, search package is not intended for refresh
    }

    public SearchPostsRVAdapter getAdapter(){
        return mAdapter;
    }
}
