package com.shmmy.examresults.searchposts;

import android.content.Context;
import android.text.Html;
import android.widget.Filter;
import android.widget.Filterable;

import com.parser.phpbb.ForumThread;
import com.parser.phpbb.Post;
import com.shmmy.examresults.PostsAdapter;
import com.shmmy.examresults.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tilemachos on 7/22/15.
 */
public class SearchPostsRVAdapter extends PostsAdapter implements Filterable {

    private final List<Post> mAllPosts;

    public SearchPostsRVAdapter() {
        mAllPosts = new ArrayList<>();
    }


    @Override
    public void setForumThreads(List<ForumThread> forumThreads) {
        super.setForumThreads(forumThreads);
        mAllPosts.clear();
        mAllPosts.addAll(super.getPosts());
    }

    @Override
    public Filter getFilter() {
        return new PostFilter(this, mAllPosts);
    }

    private static class PostFilter extends Filter {

        private final SearchPostsRVAdapter adapter;
        private final List<Post> originalList;
        private final List<Post> filteredList;

        private PostFilter(SearchPostsRVAdapter adapter, List<Post> originalList){
            this.adapter = adapter;
            this.originalList = originalList;
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            final FilterResults results = new FilterResults();
            filteredList.clear();

            if (constraint.length() == 0){
                filteredList.addAll(originalList);
            } else {
                final String filterPattern = Utils
                        .removeAccents(constraint.toString().toLowerCase().trim());
                for (Post post : originalList){
                    String plainMessage = Utils
                            .removeAccents(Html.fromHtml(post.getMessage())
                                    .toString().toLowerCase().trim());
                    if (plainMessage.contains(filterPattern))
                        filteredList.add(post);
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.setPosts((ArrayList<Post>) results.values);
            adapter.notifyDataSetChanged();
        }
    }
}
