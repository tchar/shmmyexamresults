package com.shmmy.examresults.searchposts;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.parser.phpbb.ForumThread;
import com.shmmy.examresults.R;
import com.shmmy.examresults.posts.ParsePostsActivity;
import com.shmmy.examresults.utilities.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tilemachos on 7/12/15.
 */
public class SearchPostsActivity extends AppCompatActivity implements TextWatcher{

    private final static String TAG = ParsePostsActivity.class.getSimpleName();
    private static final String ARGS_THREAD_SUBSCRIBE = TAG + "subscribed.threads";

    private List<ForumThread> mForumThreads;
    private SearchPostsController mController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_posts);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);

        Intent intent =  getIntent();
        mForumThreads = (List<ForumThread>) intent.getSerializableExtra(Constants.SEARCH_POSTS_ARG);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.postsRecyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(llm);
        SearchPostsRVAdapter searchPostsRVAdapter = new SearchPostsRVAdapter();
        recyclerView.setAdapter(searchPostsRVAdapter);

        mController = new SearchPostsController(this, searchPostsRVAdapter);

        searchPostsRVAdapter.setController(mController);


        String viewMode = getSharedPreferences(getString(R.string.preferences), MODE_PRIVATE)
                .getString(getString(R.string.preference_view_mode), "0");
        searchPostsRVAdapter.setViewMode(viewMode);

        searchPostsRVAdapter.setForumThreads(mForumThreads);
        final EditText editText = (EditText) findViewById(R.id.searchEditText);
        editText.addTextChangedListener(this);
        if(editText.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

        ImageView searchClearButton = (ImageView) findViewById(R.id.search_clear);

        // Clear search text when clear button is tapped
        searchClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
            }
        });
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle inState) {
        super.onRestoreInstanceState(inState);
        Serializable s = inState.getSerializable(ARGS_THREAD_SUBSCRIBE);
        List<ForumThread> fts = (ArrayList<ForumThread>) s;
        if (fts != null){
            mController.getAdapter().setForumThreads(fts);
            mController.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        ArrayList<ForumThread> fts = (ArrayList<ForumThread>) mForumThreads;
        outState.putSerializable(ARGS_THREAD_SUBSCRIBE, fts);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        mController.getAdapter().getFilter().filter(s);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
