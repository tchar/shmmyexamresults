package com.shmmy.examresults;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parser.phpbb.ForumThread;
import com.parser.phpbb.Post;
import com.shmmy.examresults.posts.ParsePostsActivity;
import com.shmmy.examresults.utilities.PostDateComparator;
import com.shmmy.examresults.webview.WebViewActivity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.net.URLStreamHandler;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import static android.support.v4.content.ContextCompat.startActivity;

/**
 * Created by tilemachos on 7/6/15.
 */
public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.PostViewHolder> {

    private static final String TAG = PostsAdapter.class.getSimpleName();
    private final HashMap<HashSet<Post>, ForumThread> mPostsMapList;  // To fetch thread title fast;
    private final List<Post> mPosts;
    private AbstractPostsController mController;
    private int mViewMode;

    public class PostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener
    {
        final View parentView;
        final TextView messageTextView;
        final TextView titleTextView;

        PostViewHolder(View itemView) {
            super(itemView);
            parentView = itemView;
            messageTextView = (TextView) itemView.findViewById(R.id.postMessageTextView);
            titleTextView = (TextView) itemView.findViewById(R.id.postTitleTextView);
            parentView.setOnClickListener(this);
            parentView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int pos = getLayoutPosition();
            Post post = mPosts.get(pos);
            String url = null;
            for (HashSet<Post> set : mPostsMapList.keySet()){
                if (set.contains(post)){
                    ForumThread ft = mPostsMapList.get(set);
                    if (ft != null){
                        url = ft.getThreadFullUrl() + "&p=" + post.getUrl() + "#p" + post.getUrl();
                        break;
                    }
                }
            }
            if (url != null){
                if (mController != null)
                    mController.launchBrowser(url);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            onClick(v);
            return true;
        }
    }

    public PostsAdapter(){
        mPostsMapList = new HashMap<>();
        mPosts = new ArrayList<>();
    }


    private boolean newPosts(List<ForumThread> forumThreads){
        List<Post> posts = new ArrayList<>();
        for (ForumThread ft : forumThreads)
            posts.addAll(ft.getAllPosts());

        if (posts.size() != mPosts.size())
            return true;
        if (posts.isEmpty())
            return false;

        // Find max date for posts
        Date date1 = posts.get(0).getDate();
        for (Post post : posts){
            try{
                if (post.getDate().compareTo(date1) == 1)
                    date1 = post.getDate();
            } catch (NullPointerException e){
                Log.e(TAG, e.getMessage());
            }
        }
        // Max date for mPosts
        Date date2 = mPosts.get(0).getDate();
        return !date1.equals(date2);
    }

    public void setForumThreads(List<ForumThread> forumThreads) {
        // If no new posts, return
        if (!newPosts(forumThreads))
            return;

        List<Post> posts = new ArrayList<>();
        mPostsMapList.clear();
        for (ForumThread ft : forumThreads){
            List<Post> threadPosts = ft.getAllPosts();
            HashSet<Post> set = new HashSet<>(threadPosts);
            mPostsMapList.put(set, ft);
            posts.addAll(threadPosts);
        }

        Collections.sort(posts, new PostDateComparator());
        mPosts.clear();
        mPosts.addAll(posts);
        refreshAdapter();
    }


    public void refreshAdapter(){
        notifyDataSetChanged();
    }

    public void setController(AbstractPostsController controller){
        mController = controller;
    }

    public AbstractPostsController getController(){
        return mController;
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.post_recycler_view_element, viewGroup, false);
        return new PostViewHolder(v);
    }

    private String getPostTitleText(Post post){
        String title = "";
        // Find thread title from post
        for (HashSet<Post> set : mPostsMapList.keySet()){
            if (set.contains(post)){
                ForumThread ft = mPostsMapList.get(set);
                if (ft != null){
                    title = ft.getTitle();
                    break;
                }
            }
        }
        return title;
    }

    private Spannable getFormattedMessageNormal(Post post){
        return new SpannableString(Html.fromHtml(post.getMessage()));
    }

    private Spannable getFormattedMessageCompact(Post post){
        Document doc = Jsoup.parse(post.getMessage());
        doc.getElementsByTag("dd").remove();
        return new SpannableString(Html.fromHtml(doc.toString()));
    }

    private Spannable getFormatedMessageSuperCompact(Post post){
        Document doc = Jsoup.parse(post.getMessage());
        doc.getElementsByTag("dd").remove();
        doc.getElementsByTag("br").remove();
        return new SpannableString((Html.fromHtml(doc.toString())));
    }

    public void setViewMode(String mode){
        try{
            mViewMode = Integer.valueOf(mode);
        } catch (NumberFormatException ignored){
            mViewMode = 0;
        }
    }

    public void setPosts(List<Post> posts){
        mPosts.clear();
        mPosts.addAll(posts);
    }

    public List<Post> getPosts(){
        return mPosts;
    }

    public void setBoldToTitle(PostViewHolder postViewHolder){
        postViewHolder.titleTextView.setTypeface(null, Typeface.BOLD);
    }

    @Override
    public void onBindViewHolder(PostViewHolder postViewHolder, int i) {

        Spannable message;
        if (mViewMode == 1)
            message = getFormattedMessageCompact(mPosts.get(i));
        else if (mViewMode == 2)
            message = getFormatedMessageSuperCompact(mPosts.get(i));
        else
            message = getFormattedMessageNormal(mPosts.get(i));

        for (URLSpan urlSpan: message.getSpans(0, message.length(), URLSpan.class)){
            LinkSpan linkSpan = new LinkSpan(urlSpan.getURL());
            int spanStart = message.getSpanStart(urlSpan);
            int spanEnd = message.getSpanEnd(urlSpan);
            message.setSpan(linkSpan, spanStart, spanEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            message.removeSpan(urlSpan);
        }

        postViewHolder.messageTextView.setText(message);
        postViewHolder.messageTextView.setMovementMethod(LinkMovementMethod.getInstance());
        postViewHolder.titleTextView.setText(getPostTitleText(mPosts.get(i)));
    }

    class LinkSpan extends URLSpan {

        public LinkSpan(String url) {
            super(url);
        }

        @Override
        public void onClick(View view) {
            String url = getURL();
            if (url != null) {
                Log.d("LinkSpan", "In here");
                PostsAdapter.this.getController().launchWebView(url);
            }
        }
    }
}
