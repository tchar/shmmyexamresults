package com.shmmy.examresults;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.shmmy.examresults.webview.WebViewActivity;

/**
 * Created by tilemachos on 7/24/15.
 */
public abstract class AbstractPostsController extends AbstractController{

    private final Context mContext;

    public AbstractPostsController(Context context){
        mContext = context;
    }

    public void launchBrowser(String url){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mContext.startActivity(intent);
    }

    public void launchWebView(String url){
        launchBrowser(url);
//        if (url.endsWith(".pdf")){
//            launchBrowser(url);
//        } else {
//            Intent intent = new Intent(mContext, WebViewActivity.class).putExtra(WebViewActivity.URL_KEY, url);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            mContext.startActivity(intent);
//        }
    }
}
