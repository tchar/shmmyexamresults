package com.shmmy.examresults;

/**
 * Created by tilemachos on 7/9/15.
 */
public abstract class AbstractController {

    protected abstract boolean canRefresh(int mode);
    protected abstract void doRefresh(int mode);

}
