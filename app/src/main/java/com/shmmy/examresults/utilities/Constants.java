package com.shmmy.examresults.utilities;

/**
 * Created by tilemachos on 7/9/15.
 */
public class Constants {

    public static final String THREADS_SUBSCRIBED_ARGS = "com.shmmy.examresutls.subscribed.threads";
    public static final String THREADS_SUBSCRIBED_URLS_ARG = "com.shmmy.examresults.subscribed.threads.urls";
    public static final String SEARCH_POSTS_ARG =  "com.shmmy.examresults.search_posts";
    public static final String NOTIFICATION_CLICKED_ARG = "com.shmmy.examresults.notification.clicked";


    public static final String ROOT_URL = "https://shmmy.ntua.gr/forum/";
    public static final String RESULTS_URL = "./viewforum.php?f=290";
    public static final String[] FILENAMES = {"thread1", "thread2"};
    public static final int MAX_PAGES = 14;
    public static final int MAX_THREADS = 20;
    public static final int MAX_SELECTED_THREADS = 2;
    public static final int LAST_THREAD_PAGES_REFRESH = 2;
    public static final int MIN_REQUEST_TIME = 300; // In milliseconds
    public static final int MIN_REFRESH_POSTS_TIME = 60 * 1000; // 1 Minute in milliseconds
    public static final int MIN_SWIPE_REFRESH_POSTS_TIME = 20 * 1000; // 20 seconds in milliseconds
    public static final String DATE_REGEX_MATCH = "\\b(Δευ|Τρί|Τετ|Πέμ|Παρ|Σάβ|Κυρ)\\b \\b(Ιαν|Φεβ|Μαρ|Απρ|Μάιος|Ιουν|Ιούλ|Αύγ|Σεπ|Οκτ|Νοέμ|Δεκ)\\b"
            + " \\d{2}, \\d{4} \\d{1,2}:\\d{2} \\b(am|pm)\\b";

    public static final String DATE_FORMAT = "EEE MMM dd, yyyy hh:mm a";
    public static final String[] MONTHS = {"Ιαν", "Φεβ", "Μαρ", "Απρ", "Μάιος", "Ιουν", "Ιούλ", "Αύγ", "Σεπ", "Οκτ", "Νοέμ", "Δεκ"};
    public static final String[] WEEKDAYS = {"", "Κυρ", "Δευ", "Τρί", "Τετ", "Πέμ", "Παρ", "Σάβ"};
    public static final String[] AM_PM_SYMBOLS = {"am", "pm"};

    public static final String TOPIC_NAME_STARTS_WITH = "αποτελεσματα";

    public static final String IMG_DEFAULT_NAME = "Εικόνα";

    public static final int NOTIFICATION_LED_ON_MILIS = 100;
    public static final int NOTIFICATION_LED_OFF_MILIS = 5000;

}
