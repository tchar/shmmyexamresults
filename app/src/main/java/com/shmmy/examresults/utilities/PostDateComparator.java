package com.shmmy.examresults.utilities;

import com.parser.phpbb.Post;

import java.util.Comparator;
import java.util.Date;

/**
 * Created by tilemachos on 7/10/15.
 */
public class PostDateComparator implements Comparator<Post> {
    @Override

    public int compare(Post lhs, Post rhs) {

        Date d1 = lhs.getDate();
        Date d2 = rhs.getDate();

        if (d1 == null && d2 == null)
            return 0;
        if (d1 == null)
            return 1;
        if (d2 == null)
            return -1;

        return d2.compareTo(d1);
    }
}
