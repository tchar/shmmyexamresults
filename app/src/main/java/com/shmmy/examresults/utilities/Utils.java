package com.shmmy.examresults.utilities;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.accessibility.AccessibilityManager;

import com.shmmy.examresults.R;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by tilemachos on 7/12/15.
 */
public class Utils {

    private final static Map<Character, Character> MAP_NORM;
    static { // Greek characters normalization
        MAP_NORM = new HashMap<>();
        MAP_NORM.put('ά', 'α');
        MAP_NORM.put('έ', 'ε');
        MAP_NORM.put('ί', 'ι');
        MAP_NORM.put('ό', 'ο');
        MAP_NORM.put('ύ', 'υ');
        MAP_NORM.put('ή', 'η');
        MAP_NORM.put('ς', 'σ');
        MAP_NORM.put('ώ', 'ω');
        MAP_NORM.put('Ά', 'α');
        MAP_NORM.put('Έ', 'ε');
        MAP_NORM.put('Ί', 'ι');
        MAP_NORM.put('Ό', 'ο');
        MAP_NORM.put('Ύ', 'υ');
        MAP_NORM.put('Ή', 'η');
        MAP_NORM.put('Ώ', 'ω');
    }

    private final static int[] intervals = new int[] {  1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                                                        15, 20, 25, 30, 35, 40, 45, 50, 55, 60,
                                                        120, 180, 240, 300, 360, 420, 480, 540, 600, 660,
                                                        720, 780, 840, 900, 960, 1020, 1080, 1140, 1200, 1260,
                                                        1320, 1380, 1440};

    public static String removeAccents(String s) {
        if (s == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder(s);

        for(int i = 0; i < s.length(); i++) {
            Character c = MAP_NORM.get(sb.charAt(i));
            if(c != null) {
                sb.setCharAt(i, c);
            }
        }
        return sb.toString();
    }

    public static int getToolbarHeight(Context context) {
        final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                new int[]{R.attr.actionBarSize});
        int toolbarHeight = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        return toolbarHeight;
    }

    public static int getIntervalFromPercentageRounded(int p){
        int interval = 60 * intervals[p*42/100];
        return interval;
    }

    public static int getIntervalFromPercentage(int p){
        double expBase = 1.03;
        double expMin = expBase;
        double expMax = Math.pow(expBase, 100);
        double expVal = Math.pow(expBase, p);
        double expRange = expMax - expMin;

        double intervalMax = 12 * 60;  // 12 hours in minutes
        double intervalMin = 1;   // 1 minute
        double interval;

        if (expRange == 0)
            interval = intervalMin;
        else
        {
            double intervalRange = (intervalMax - intervalMin);
            interval = (((expVal - expMin) * intervalRange) / expRange) + intervalMin;
        }

        if (interval < intervalMin)
            interval = intervalMin;
        else if (interval > intervalMax)
            interval = intervalMax;
        interval = interval * 60; // Interval in seconds
        return (int) interval;
    }

    public static Snackbar getHackedSnackbar(View view, String msg, int duration){
        Snackbar snackbar = Snackbar.make(view, msg, duration);
        try{
            Field mAccessibilityManagerField = BaseTransientBottomBar.class.getDeclaredField("mAccessibilityManager");
            mAccessibilityManagerField.setAccessible(true);
            AccessibilityManager accessibilityManager = (AccessibilityManager) mAccessibilityManagerField.get(snackbar);
            Field mIsEnabledField = AccessibilityManager.class.getDeclaredField("mIsEnabled");
            mIsEnabledField.setAccessible(true);
            mIsEnabledField.setBoolean(accessibilityManager, false);
            mAccessibilityManagerField.set(snackbar, accessibilityManager);
        } catch (Exception e){
            Log.d("Snackbar", "Reflection error: " + e.toString());
        }
        return snackbar;
    }
}
