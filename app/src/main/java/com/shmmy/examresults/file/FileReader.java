package com.shmmy.examresults.file;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.parser.phpbb.ForumThread;
import com.shmmy.examresults.UpdateOperationsCallback;
import com.shmmy.examresults.utilities.Constants;

import org.apache.commons.collections4.iterators.ArrayIterator;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tilemachos on 7/8/15.
 */
public class FileReader extends AsyncTask<Void, Void, List<ForumThread>> {

    private static final String TAG = FileReader.class.getSimpleName();
    private final Context mContext;
    private final UpdateOperationsCallback mCallback;
    private final ArrayIterator<String> fileNamesIter = new ArrayIterator<>(Constants.FILENAMES);

    public FileReader(Context context, UpdateOperationsCallback callback){
        mContext = context;
        mCallback = callback;
    }

    private String getNextFileName(){
        if (fileNamesIter.hasNext())
            return fileNamesIter.next();
        else
            return null;
    }

    private ForumThread readThreadFromFile(String fileName){
        ForumThread thread = null;
        try {
            FileInputStream fis = mContext.openFileInput(fileName);
            ObjectInputStream is = new ObjectInputStream(fis);
            thread = (ForumThread) is.readObject();
            is.close();
            fis.close();
        } catch (IOException | ClassNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }
        return thread;
    }


    @Override
    protected List<ForumThread> doInBackground(Void... params) {
        List<ForumThread> forumThreads = new ArrayList<>();
        ForumThread forumThread;
        String fileName;

        while((fileName = getNextFileName()) != null){
            forumThread = readThreadFromFile(fileName);
            if (forumThread != null)
                forumThreads.add(forumThread);
        }

        return forumThreads;
    }

    @Override
    protected void onPostExecute(List<ForumThread> forumThreads){
        mCallback.onFileReadComplete(forumThreads);
    }
}
