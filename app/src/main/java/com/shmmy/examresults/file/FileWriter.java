package com.shmmy.examresults.file;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.parser.phpbb.ForumThread;
import com.shmmy.examresults.UpdateOperationsCallback;
import com.shmmy.examresults.utilities.Constants;

import org.apache.commons.collections4.iterators.ArrayIterator;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;


/**
 * Created by tilemachos on 7/8/15.
 */

public class FileWriter extends AsyncTask<ForumThread, Void, Void> {

    private static final String TAG = FileWriter.class.getSimpleName();
    private final Context mContext;
    private final UpdateOperationsCallback mCallback;
    private final ArrayIterator<String> fileNamesIter = new ArrayIterator<>(Constants.FILENAMES);

    public FileWriter(Context context, UpdateOperationsCallback callback){
        mContext = context;
        mCallback = callback;
    }


    /**
     * @return      Returns the next fileName if exists
     *              or null if it doesn't exist.
     */
    private String getNextFileName(){
        if (fileNamesIter.hasNext())
            return fileNamesIter.next();
        else
            return null;
    }

    private void writeThreadToFile(ForumThread thread){
        try {
            String fileName = getNextFileName();
            if (fileName == null)
                return;
            Log.d(TAG, "Writing to file " + fileName);
            FileOutputStream fos = mContext.openFileOutput(fileName, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(thread);
            os.close();
            fos.close();

            Log.d(TAG, "Finished writing");
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
    }


    @Override
    protected Void doInBackground(ForumThread... fts) {

        String fileName;
        while ((fileName = getNextFileName()) != null){
            mContext.deleteFile(fileName);
        }
        fileNamesIter.reset();

        for (ForumThread ft : fts)
            writeThreadToFile(ft);

        return null;
    }

    @Override
    protected void onPostExecute(Void v){
        mCallback.onFileWriteComplete();
    }

}
