package com.shmmy.examresults.settings;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import com.shmmy.examresults.R;
import com.shmmy.examresults.service.UpdateService;
import com.shmmy.examresults.utilities.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by tilemachos on 7/15/15.
 */
public class SettingsActivity extends PreferenceActivity{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SettingsPreferenceFragment spf = new SettingsPreferenceFragment();
        getFragmentManager().beginTransaction().replace(android.R.id.content
                , spf).commit();
    }




    public static class SettingsPreferenceFragment extends PreferenceFragment implements
            SharedPreferences.OnSharedPreferenceChangeListener {


        private static final String TAG = SettingsPreferenceFragment.class.getSimpleName();

        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            getPreferenceManager().setSharedPreferencesName(getString(R.string.preferences));
            addPreferencesFromResource(R.xml.preferences);
        }

        @Override
        public void onResume() {
            super.onResume();
            // Set up a listener whenever a key changes
            getPreferenceScreen().getSharedPreferences()
                    .registerOnSharedPreferenceChangeListener(this);
            setSeekBarSummary(getStringFromInterval());
            setQuietFromSummary();
            setQuietToSummary();
        }

        @Override
        public void onPause() {
            super.onPause();
            // Unregister the listener whenever a key changes
            getPreferenceScreen().getSharedPreferences()
                    .unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

            View parentView = getActivity().getWindow().getDecorView()
                                .findViewById(android.R.id.content);
            String snackbarText = "";

            if (key.equals(getString(R.string.preference_notifications)))
            {
                if (sharedPreferences.getBoolean(key, false)){
                    UpdateService.startUpdateService(this.getActivity());
                    snackbarText = getString(R.string.notifications_enabled);
                }
                else{
                    UpdateService.stopUpdateService(this.getActivity());
                    snackbarText = getString(R.string.notifications_disabled);
                }
            } else if (key.equals(getString(R.string.preference_notifications_interval))){
                String strInterval = getStringFromInterval();
                setSeekBarSummary(strInterval);
                UpdateService.setUpdateServiceInterval(this.getActivity());
                snackbarText = getString(R.string.settings_interval_changed) + " " + strInterval;
            } else if (key.equals(getString(R.string.preference_view_mode))){
                // This result code is used to refresh adapter in main activity
                this.getActivity().setResult(RESULT_OK);
                snackbarText = getString(R.string.settings_view_mode_changed) + " " + getViewMode();
            } else if (key.equals(getString(R.string.preference_quiet_hours))){
                if (sharedPreferences.getBoolean(key, false))
                    snackbarText = getString(R.string.settings_quiet_hours_enabled);
                else
                    snackbarText = getString(R.string.settings_quiet_hours_disabled);
            } else if (key.equals(getString(R.string.preference_quiet_hours_from))){
                setQuietFromSummary();
                String def = getString(R.string.preference_quiet_hours_from_default);
                String time = sharedPreferences.getString(key, def);
                snackbarText = getString(R.string.settings_quiet_from_changed) + " "
                                    + getFormatedTime(time, def);
            } else if (key.equals(getString(R.string.preference_quiet_hours_to))){
                setQuietToSummary();
                String def = getString(R.string.preference_quiet_hours_to_default);
                String time = sharedPreferences.getString(key, def);
                snackbarText = getString(R.string.settings_quiet_to_changed) + " "
                        + getFormatedTime(time, def);
            }

            Snackbar snackbar =  Utils.getHackedSnackbar(parentView, snackbarText, Snackbar.LENGTH_LONG);

            View snackbarView = snackbar.getView();
            TextView tv = (TextView) snackbarView
                    .findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
            snackbar.show();
        }

        private String getViewMode(){
            SharedPreferences sharedPreferences = getActivity()
                    .getSharedPreferences(getString(R.string.preferences), MODE_PRIVATE);
            // Get view mode from shared preferences
            String key = getString(R.string.preference_view_mode);
            String viewMode = sharedPreferences.getString(key, "0");
            String[] modes = getActivity().getResources().getStringArray(R.array.view_mode);
            String[] modesVals = getActivity().getResources().getStringArray(R.array.view_mode_values);
            for (int i = 0; i < modesVals.length; i++){
                if (modesVals[i].equals(viewMode))
                    return modes[i];
            }
            return getString(R.string.settings_view_mode_normal);
        }

        private String getFormatedTime(String time, String def){
            String ret;
            DateFormat parserDF = new SimpleDateFormat("HH:mm");
            DateFormat formatterDF;
            try {
                if (android.text.format.DateFormat.is24HourFormat(getActivity()))
                    formatterDF = parserDF;
                else
                    formatterDF = new SimpleDateFormat("hh:mm a");

                ret = formatterDF.format(parserDF.parse(time));
            } catch (ParseException ignored) {
                ret = def;
            }
            return ret;
        }

        private void setQuietFromSummary(){
            String quietFromKey = getString(R.string.preference_quiet_hours_from);
            SharedPreferences sharedPreferences = this.getActivity()
                    .getSharedPreferences(getString(R.string.preferences), MODE_PRIVATE);
            String def = getString(R.string.preference_quiet_hours_from_default);
            String from = sharedPreferences.getString(quietFromKey, def);
            from = getFormatedTime(from, def);

            getPreferenceManager().findPreference(quietFromKey).setSummary(from);
        }

        private void setQuietToSummary(){
            String quietToKey = getString(R.string.preference_quiet_hours_to);
            SharedPreferences sharedPreferences = this.getActivity()
                    .getSharedPreferences(getString(R.string.preferences), MODE_PRIVATE);
            String def = getString(R.string.preference_quiet_hours_to_default);
            String to = sharedPreferences.getString(quietToKey, def);
            to = getFormatedTime(to, def);

            getPreferenceManager().findPreference(quietToKey).setSummary(to);
        }

        private void setSeekBarSummary(String strInterval){
            String seekBarKey = getString(R.string.preference_notifications_interval);
            String summary = getString(R.string.settings_interval_seekbar_summary);
            summary += " " + strInterval;
            getPreferenceManager().findPreference(seekBarKey).setSummary(summary);
        }

        private String getStringFromInterval(){
            String seekBarKey = getString(R.string.preference_notifications_interval);
            SharedPreferences sharedPreferences = this.getActivity()
                    .getSharedPreferences(getString(R.string.preferences), MODE_PRIVATE);
            int intervalPercentage = sharedPreferences.getInt(seekBarKey, 50);
            int i = Utils.getIntervalFromPercentageRounded(intervalPercentage);

            String s = "";

            if (i < 60 * 60){
                int mins = i/60;
                String minsStr;
                if (mins == 1)
                    minsStr = mins + " " + getString(R.string.minute);
                else
                    minsStr = mins + " " + getString(R.string.minutes);
                s += minsStr + ".";
            }
            else{
                int hours = i/(60 *60);
                int mins = (i - (hours * 60 * 60))/60;
                String hoursStr;
                if (hours == 1)
                    hoursStr = hours + " " + getString(R.string.hour);
                else
                    hoursStr = hours + " " + getString(R.string.hours);

                String minsStr;
                if (mins == 0)
                    minsStr = "";
                else if (mins == 1)
                    minsStr = ", " + mins + " " + getString(R.string.minute);
                else
                    minsStr = ", " + mins + " " + getString(R.string.minutes);
                s += hoursStr + minsStr + ".";
            }
            return s;
        }
    }
}
