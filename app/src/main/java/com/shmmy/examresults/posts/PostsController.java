package com.shmmy.examresults.posts;

import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;

import com.parser.phpbb.ForumThread;
import com.shmmy.examresults.AbstractPostsController;
import com.shmmy.examresults.UpdateOperationsCallback;
import com.shmmy.examresults.file.FileWriter;
import com.shmmy.examresults.parser.PostsParser;
import com.shmmy.examresults.utilities.Constants;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by tilemachos on 7/7/15.
 */
public class PostsController extends AbstractPostsController
        implements SwipeRefreshLayout.OnRefreshListener, UpdateOperationsCallback {

    private static final String TAG = PostsController.class.getSimpleName();
    private final static int MODE_DO_REFRESH = 1;
    private final static int MODE_SWIPE_REFRESH = 2;
    private final SwipeRefreshLayout mSwipeRefreshLayout;
    private final PostsRVAdapter mPostsRVAdapter;

    private final GregorianCalendar mLastRefresh;
    private final List<ForumThread> mSubscribedThreads;
    private final ParsePostsActivity mActivity;


    public PostsController(ParsePostsActivity activity,
                           SwipeRefreshLayout swipeRefreshLayout,
                           PostsRVAdapter postsRVAdapter){
        super(activity);
        mActivity = activity;
        mSwipeRefreshLayout = swipeRefreshLayout;
        mPostsRVAdapter = postsRVAdapter;
        mSubscribedThreads = new ArrayList<>();
        mLastRefresh = new GregorianCalendar(1990, 1, 1);
    }

    private void setRefreshing(boolean bool){
        if (bool){
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
        }
        else{
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });
        }
    }

    @Override
    public void onRefresh() {
        // Refresh items
        Log.d(TAG, "Start onRefresh");
        doRefresh(MODE_SWIPE_REFRESH);
        Log.d(TAG, "End onRefresh");
    }

    @Override
    protected boolean canRefresh(int mode){
        int compare;
        if (mode == MODE_SWIPE_REFRESH)
            compare = Constants.MIN_SWIPE_REFRESH_POSTS_TIME;
        else
            compare = Constants.MIN_REFRESH_POSTS_TIME;

        GregorianCalendar gc = new GregorianCalendar();
        if (gc.getTimeInMillis() - mLastRefresh.getTimeInMillis() < compare){
            setRefreshing(false);
            return false;
        }
        return true;
    }

    @Override
    protected void doRefresh(int mode) {
        if (!canRefresh(mode)) {
            if (mode == MODE_SWIPE_REFRESH)
                mActivity.tooFastRefreshSnackbar();
            return;
        }
        setRefreshing(true);
        ForumThread [] fta = new ForumThread[mSubscribedThreads.size()];
        fta = mSubscribedThreads.toArray(fta);
        new PostsParser(this).execute(fta);
    }

    private void changeAdapterData(List<ForumThread> fts){
        mPostsRVAdapter.setForumThreads(fts);
    }

    private void writeToFile(List<ForumThread> fts){
        ForumThread[] fta = new ForumThread[fts.size()];
        fta = fts.toArray(fta);
        new FileWriter(mActivity, this).execute(fta);
    }

    @Override
    public void onItemsLoadComplete(List<ForumThread> fts) {
        // Update the adapter
        Log.d(TAG, "Start onItemsLoadComplete");
        mLastRefresh.setTime(new Date());
        mActivity.saveLastRefresh(mLastRefresh);
        changeAdapterData(fts);
        writeToFile(fts);
        mSubscribedThreads.clear();
        mSubscribedThreads.addAll(fts);
        // Stop refresh animation
        setRefreshing(false);
        Log.d(TAG, "End onItemsLoadComplete");
    }

    @Override
    public void onFileReadComplete(List<ForumThread> fts) {
        changeAdapterData(fts);
        mSubscribedThreads.clear();
        mSubscribedThreads.addAll(fts);
        doRefresh(MODE_DO_REFRESH);
    }

    @Override
    public void onFileWriteComplete(){

    }

    public void setSubscribedThreads(List<ForumThread> fts){
        changeAdapterData(fts);
        onFileReadComplete(fts);
    }

    public void resetRefresh(){
        mLastRefresh.set(1990, 1, 1);
    }

    public List<ForumThread> getSubscribedThreads(){
        return mSubscribedThreads;
    }

    public void refreshAdapter(){
        mPostsRVAdapter.refreshAdapter();
    }

    public void setLastRefresh(long timeInMilis){
        mLastRefresh.setTimeInMillis(timeInMilis);
    }

    public final GregorianCalendar getLastRefresh(){
        return this.mLastRefresh;
    }

    public void setViewMode(String mode){
        mPostsRVAdapter.setViewMode(mode);
    }

    public GregorianCalendar getMaxDateViewed(){
        return mPostsRVAdapter.getMaxDateViewed();
    }

    public void setMaxDate(long timeInMilis){
        mPostsRVAdapter.setMaxDate(timeInMilis);
    }

    public void resetMaxDate(){
        mPostsRVAdapter.resetMaxDate();
    }

    public void scrollToFirstItem(){
        mPostsRVAdapter.scrollToFirstItem();
    }

    public void newPostsSnackBar(){
        mActivity.newPostsSnackBar();
    }

    public void noNewPostsSnackBar(){
        mActivity.noNewPostsSnackBar();
    }

}

