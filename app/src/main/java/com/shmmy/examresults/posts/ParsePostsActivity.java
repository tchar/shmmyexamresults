package com.shmmy.examresults.posts;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.parser.phpbb.ForumThread;
import com.shmmy.examresults.R;
import com.shmmy.examresults.file.FileReader;
import com.shmmy.examresults.searchposts.SearchPostsActivity;
import com.shmmy.examresults.service.UpdateService;
import com.shmmy.examresults.settings.SettingsActivity;
import com.shmmy.examresults.threadsubscribe.SubscribeThreadActivity;
import com.shmmy.examresults.utilities.Constants;
import com.shmmy.examresults.utilities.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class ParsePostsActivity extends AppCompatActivity
        implements FloatingActionButton.OnClickListener {

    private final static String TAG = ParsePostsActivity.class.getSimpleName();
    private final static int REQUEST_SUBSCRIBE_THREAD = 4316;
    private final static int REQUEST_SETTINGS = 4317;


    private PostsController mPostsController;
    private CoordinatorLayout mParentLayout;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        boolean clickedNotification = intent.getBooleanExtra(
                                        Constants.NOTIFICATION_CLICKED_ARG, false);

        setContentView(R.layout.activity_parse_posts);

        mParentLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.light_bulb_white);
        mToolbar.setTitle(R.string.title_activity_parse_posts);
        setSupportActionBar(mToolbar);

        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.postsRecyclerView);
        int duration = getResources().getInteger(R.integer.scroll_duration);
        ScrollingLinearLayoutManager sllm =
                new ScrollingLinearLayoutManager(this, LinearLayoutManager.VERTICAL, false, duration);

        recyclerView.setLayoutManager(sllm);
        PostsRVAdapter postsRVAdapter = new PostsRVAdapter(recyclerView, clickedNotification);
        recyclerView.setAdapter(postsRVAdapter);

        SwipeRefreshLayout srl = (SwipeRefreshLayout) findViewById(R.id.postsRefreshLayout);
        srl.setColorSchemeResources(R.color.shmmy_blue);
        mPostsController = new PostsController(this, srl, postsRVAdapter);
        srl.setOnRefreshListener(mPostsController);

        postsRVAdapter.setController(mPostsController);

        processBundle(savedInstanceState);
    }

    private void processSharesPreferences(){

        SharedPreferences sharedPreferences =
                getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE);

        // Get last update from shared preferences
        long lastUpdate = sharedPreferences.getLong(getString(R.string.preference_last_update), -1);
        if (lastUpdate != -1){
            mPostsController.setLastRefresh(lastUpdate);
        }

        // Get max date viewed from shared preferences
        long maxDate = sharedPreferences.getLong(getString(R.string.preference_max_date_viewed), -1);
        if (maxDate != -1){
            mPostsController.setMaxDate(maxDate);
        }

        // Get view mode from shared preferences
        String viewMode = sharedPreferences
                .getString(getString(R.string.preference_view_mode), "0");

        mPostsController.setViewMode(viewMode);
    }

    private void processBundle(Bundle bundle){

        processSharesPreferences();

        if (bundle == null){
            new FileReader(this, mPostsController).execute();
            return;
        }

        Serializable s = bundle.getSerializable(Constants.THREADS_SUBSCRIBED_ARGS);
        List<ForumThread> fts = (ArrayList<ForumThread>) s;
        if (fts != null){
            mPostsController.setSubscribedThreads(fts);
        } else{
            new FileReader(this, mPostsController).execute();
        }
    }

    public void saveLastRefresh(GregorianCalendar gc){
        SharedPreferences sharedPreferences =
                getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(getString(R.string.preference_last_update), gc.getTime().getTime());
        editor.apply();
    }

    private void startSearchPostsActivity(){
        Intent intent = new Intent(getApplicationContext(), SearchPostsActivity.class);
        Bundle bundle = new Bundle();
        ArrayList<ForumThread> fts = (ArrayList<ForumThread>) mPostsController.getSubscribedThreads();
        bundle.putSerializable(Constants.SEARCH_POSTS_ARG, fts);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void startThreadsSusbscribeActivity(){
        Intent intent = new Intent(getApplicationContext(), SubscribeThreadActivity.class);
        List<ForumThread> forumThreads = mPostsController.getSubscribedThreads();
        ArrayList<String> urls = new ArrayList<>();
        for (ForumThread ft : forumThreads)
            urls.add(ft.getThreadSubUrl());
        intent.putStringArrayListExtra(Constants.THREADS_SUBSCRIBED_URLS_ARG, urls);
        startActivityForResult(intent, REQUEST_SUBSCRIBE_THREAD);
    }

    private void startSettingsActivity(){
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivityForResult(intent, REQUEST_SETTINGS);
    }

    public void newPostsSnackBar(){
        Utils.getHackedSnackbar(mParentLayout, getString(R.string.new_exam_results), Snackbar.LENGTH_LONG)
                .show();
    }

    public void noNewPostsSnackBar() {
        Utils.getHackedSnackbar(mParentLayout, getString(R.string.no_new_exam_results), Snackbar.LENGTH_LONG)
                .show();
    }

    public void tooFastRefreshSnackbar(){
        Utils.getHackedSnackbar(mParentLayout, getString(R.string.too_fast_refresh), Snackbar.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        SharedPreferences sharedPreferences =
                getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(getString(R.string.preference_max_date_viewed),
                mPostsController.getMaxDateViewed().getTime().getTime());
        editor.apply();
        super.onPause();
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle inState) {
        super.onRestoreInstanceState(inState);
        processBundle(inState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        ArrayList<ForumThread> fts = (ArrayList<ForumThread>) mPostsController.getSubscribedThreads();
        outState.putSerializable(Constants.THREADS_SUBSCRIBED_ARGS, fts);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == REQUEST_SUBSCRIBE_THREAD) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                mPostsController.resetRefresh();
                mPostsController.resetMaxDate();
                new FileReader(this, mPostsController).execute();
            }
        } else if (requestCode == REQUEST_SETTINGS) {
            // If view mode changed == RESULT_OK
            if (resultCode == RESULT_OK) {
                String viewMode = getSharedPreferences(getString(R.string.preferences), MODE_PRIVATE)
                        .getString(getString(R.string.preference_view_mode), "0");
                mPostsController.setViewMode(viewMode);
                mPostsController.refreshAdapter();
            }
            assignNotificationsIcon(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        assignNotificationsIcon(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_subscribe_thread){
            startThreadsSusbscribeActivity();
            return true;
        }
        if (id == R.id.action_settings) {
            startSettingsActivity();
            return true;
        }
        if (id == R.id.action_search){
            startSearchPostsActivity();
            return true;
        }
        if (id == R.id.action_about){
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_about);
            String version;
            try {
                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                version = pInfo.versionName;
            } catch (PackageManager.NameNotFoundException ignored) {
                version = "";
            }
            String versionText = getString(R.string.dialog_version, version);
            ((TextView) dialog.findViewById(R.id.textViewVersion)).setText(versionText);
            dialog.setTitle(R.string.dialog_about);
            dialog.show();
        }
        if (id == android.R.id.home){
            assignNotificationsIcon(true);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void notificationsChanged(boolean notifications){
        String snackBarText = null;
        if (notifications){
            snackBarText = getString(R.string.notifications_enabled);
            UpdateService.startUpdateService(this);
        } else {
            snackBarText = getString(R.string.notifications_disabled);
            UpdateService.stopUpdateService(this);
        }

        Utils.getHackedSnackbar(mParentLayout, snackBarText, Snackbar.LENGTH_LONG).show();
//        Snackbar.make(mParentLayout, snackBarText, Snackbar.LENGTH_LONG)
//                .show();
    }

    private void assignNotificationsIcon(boolean revert){
        SharedPreferences sp = getSharedPreferences(getString(R.string.preferences), MODE_PRIVATE);
        boolean notifications = sp.getBoolean(getString(R.string.preference_notifications), false);
        if (revert){
            notifications = !notifications;
            sp.edit().putBoolean(getString(R.string.preference_notifications), notifications).apply();
            notificationsChanged(notifications);
        }
        if (notifications){
            mToolbar.setNavigationIcon(R.drawable.light_bulb_white);
        } else {
            mToolbar.setNavigationIcon(R.drawable.light_bulb);
        }
    }

    @Override
    public void onClick(View v) {
        mPostsController.scrollToFirstItem();
    }
}
