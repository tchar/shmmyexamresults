package com.shmmy.examresults.posts;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import com.shmmy.examresults.PostsAdapter;
import java.util.GregorianCalendar;

/**
 * Created by tilemachos on 7/6/15.
 */
public class PostsRVAdapter extends PostsAdapter {

    private static final String TAG = PostsRVAdapter.class.getSimpleName();
    private final RecyclerView mRecyclerView;
    private final GregorianCalendar mMaxDate;
    private final GregorianCalendar mMaxDateViewed;
    private boolean mClickedNotification;


    public PostsRVAdapter(RecyclerView rv, boolean clickedNotification) {
        mRecyclerView = rv;
        mClickedNotification = clickedNotification;
        mRecyclerView.addOnScrollListener(new PostsRVScrollListener(this));
        mMaxDate = new GregorianCalendar(1990, 1, 1);
        mMaxDateViewed = new GregorianCalendar();
        mMaxDateViewed.setTimeInMillis(mMaxDate.getTimeInMillis());
    }

    private int getPositionToScroll(){

        if (mClickedNotification){
            mClickedNotification = false;
            return 0;
        }

        int position = getItemCount() - 1;
        for (int i = 0; i < getItemCount(); i++){
            try {
                if (getPosts().get(i).getDate().compareTo(mMaxDate.getTime()) != 1){
                    position = i;
                    break;
                }
            } catch (NullPointerException e){
                Log.e(TAG, e.getMessage());
            }
        }
        return position;
    }

    public void scrollToFirstItem(){
        mRecyclerView.smoothScrollToPosition(0);
    }

    @Override
    public void refreshAdapter(){
        super.refreshAdapter();
        int position = getPositionToScroll();
        mRecyclerView.scrollToPosition(0);
        if (position != 0 && position != -1 && getController() != null)
            ((PostsController) getController()).newPostsSnackBar();
    }

    public GregorianCalendar getMaxDateViewed(){
        return mMaxDateViewed;
    }

    public void setMaxDate(long timeInMilis){
        mMaxDate.setTimeInMillis(timeInMilis);
        mMaxDateViewed.setTimeInMillis(timeInMilis);
        Log.d(TAG, "Max date : " + mMaxDate.getTime());
    }

    public void setMaxDateViewed(long timeInMilis){
        mMaxDateViewed.setTimeInMillis(timeInMilis);
    }

    public void resetMaxDate(){
        mMaxDate.set(1990, 1, 1);
        mMaxDateViewed.setTimeInMillis(mMaxDate.getTimeInMillis());
    }

    @Override
    public void onBindViewHolder(PostViewHolder postViewHolder, int i) {
        super.onBindViewHolder(postViewHolder, i);

        try {
            if (getPosts().get(i).getDate().compareTo(mMaxDate.getTime()) == 1){
                super.setBoldToTitle(postViewHolder);
            }
        } catch (NullPointerException e){
            Log.e(TAG, e.getMessage());
        }
    }
}
