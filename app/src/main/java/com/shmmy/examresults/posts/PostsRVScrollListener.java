package com.shmmy.examresults.posts;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.parser.phpbb.Post;

import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by tilemachos on 7/23/15.
 */
public class PostsRVScrollListener extends RecyclerView.OnScrollListener {

    private static final String TAG = PostsRVScrollListener.class.getSimpleName();
    private final PostsRVAdapter mPostsRVAdapter;

    public PostsRVScrollListener(PostsRVAdapter adapter){
        mPostsRVAdapter = adapter;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        LinearLayoutManager llm = (LinearLayoutManager) recyclerView.getLayoutManager();
        int itemViewedPosition = llm.findFirstCompletelyVisibleItemPosition();
        if (itemViewedPosition == -1)
            itemViewedPosition = llm.findFirstVisibleItemPosition();
        if (itemViewedPosition == -1)
            return;

        List<Post> posts = mPostsRVAdapter.getPosts();
        GregorianCalendar maxDateViewed = mPostsRVAdapter.getMaxDateViewed();
        try{
            if (posts.get(itemViewedPosition).getDate().compareTo(maxDateViewed.getTime()) == 1){
                mPostsRVAdapter.setMaxDateViewed(posts.get(itemViewedPosition).getDate().getTime());
            }
        } catch (NullPointerException e){
            Log.e(TAG, e.getMessage());
        }
    }
}
