package com.shmmy.examresults.service;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.Task;
import com.google.android.gms.gcm.TaskParams;
import com.parser.phpbb.ForumThread;
import com.parser.phpbb.PhpBB3Parser;
import com.parser.phpbb.Post;
import com.shmmy.examresults.R;
import com.shmmy.examresults.posts.ParsePostsActivity;
import com.shmmy.examresults.utilities.Constants;
import com.shmmy.examresults.utilities.Utils;

import org.apache.commons.collections4.iterators.ArrayIterator;
import org.joda.time.LocalTime;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by tilemachos on 7/13/15.
 */
public class UpdateService extends GcmTaskService {

    private final static String TAG = UpdateService.class.getSimpleName();
    private static int notificationId = 1342;
    private final static int STATUS_NEW_POSTS = 0;
    private final static int STATUS_NO_NEW_POSTS = 1;
    private final static int STATUS_FAIL_READ = 2;
    private final static int STATUS_FAIL_WRITE = 3;

    @Override
    public int onRunTask(TaskParams taskParams) {
        int ret;
        int status;

        Log.d(TAG, "Start onRunTask");
        try{
            if (checkForNewPosts(readThreads()))
                status = STATUS_NEW_POSTS;
            else
                status = STATUS_NO_NEW_POSTS;
        } catch (ReadThreadsException e){
            status = STATUS_FAIL_READ;
        } catch (WriteThreadsException e){
            status = STATUS_FAIL_WRITE;
        }

        switch (status){
            case STATUS_NEW_POSTS:
                ret = GcmNetworkManager.RESULT_SUCCESS;
                notifyUser(getString(R.string.new_exam_results), 0);
                break;

            case STATUS_NO_NEW_POSTS:
                ret = GcmNetworkManager.RESULT_SUCCESS;
                //notifyUser(getString(R.string.no_new_exam_results), 1);
                break;

            case STATUS_FAIL_READ:
                ret = GcmNetworkManager.RESULT_FAILURE;
                break;

            case STATUS_FAIL_WRITE:
                ret = GcmNetworkManager.RESULT_RESCHEDULE;
                break;

            default:
                ret = GcmNetworkManager.RESULT_FAILURE;
        }

        Log.d(TAG, "End onRunTask");
        return ret;
    }

    @Override
    public void onInitializeTasks() {
        super.onInitializeTasks();
        if (getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
                .getBoolean(getString(R.string.preference_notifications), false)){
            setTask(this);
        }
    }

    private boolean shouldPlaySound(){
        SharedPreferences sharedPreferences = getSharedPreferences(
                getString(R.string.preferences), MODE_PRIVATE);
        boolean quietHours = sharedPreferences.getBoolean(
                getString(R.string.preference_quiet_hours), false);
        if (!quietHours)
            return true;

        String def = getString(R.string.preference_quiet_hours_from_default);
        String from = sharedPreferences.getString(
                getString(R.string.preference_quiet_hours_from), def);
        def = getString(R.string.preference_quiet_hours_to_default);
        String to = sharedPreferences.getString(
                getString(R.string.preference_quiet_hours_to), def);

        try {
            LocalTime fromTime = new LocalTime(from);
            LocalTime toTime = new LocalTime(to);
            LocalTime now = new LocalTime();
            if (toTime.isBefore(fromTime)){
                LocalTime temp = fromTime;
                fromTime = toTime;
                toTime = temp;
                return (fromTime.isBefore(now) && now.isBefore(toTime));
            } else {
                return !(fromTime.isBefore(now) && now.isBefore(toTime));
            }
        } catch (IllegalArgumentException ignored) {
            return true;
        }
    }

    private void notifyUser(String message, int type){
        Intent intent =  new Intent(this, ParsePostsActivity.class);
        intent.putExtra(Constants.NOTIFICATION_CLICKED_ARG, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pedningIntent = PendingIntent.getActivity(this, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder nb = new NotificationCompat.Builder(this)
                                            .setSmallIcon(R.drawable.light_bulb_white)
                                            .setContentTitle(this.getString(R.string.new_posts))
                                            .setContentText(message)
                                            .setContentIntent(pedningIntent)
                                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                                            .setLights(Color.WHITE,
                                                      Constants.NOTIFICATION_LED_ON_MILIS,
                                                      Constants.NOTIFICATION_LED_OFF_MILIS)
                                            .setAutoCancel(true);
        if (shouldPlaySound()){
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            nb.setSound(alarmSound);
        }

        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId + type, nb.build());
    }

    private Date getMaxDate(List<ForumThread> forumThreads){
        Date date = null;
        List<Post> posts = new ArrayList<>();
        for (ForumThread ft : forumThreads)
            posts.addAll(ft.getAllPosts());

        for (Post post : posts){
            if (date == null)
                date = post.getDate();
            try{
                if (date.compareTo(post.getDate()) == -1)
                    date = post.getDate();
            } catch (NullPointerException e){
                Log.e(TAG, e.getMessage());
            }
        }
        return date;
    }

    private boolean checkForNewPosts(List<ForumThread> forumThreads) throws WriteThreadsException{
        Date date1 = getMaxDate(forumThreads);
        new PhpBB3Parser().parsePosts(forumThreads);
        Date date2 = getMaxDate(forumThreads);
        if (date2.compareTo(date1) == 1){
            saveThreads(forumThreads);
            return true;
        } else {
            return false;
        }
    }

    private void saveThread(ForumThread thread, String fileName) throws IOException{
            deleteFile(fileName);

            Log.d(TAG, "Writing to file " + fileName);
            FileOutputStream fos = openFileOutput(fileName, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(thread);
            os.close();
            fos.close();

            Log.d(TAG, "Finished writing");
    }

    private void saveThreads(List<ForumThread> fts) throws WriteThreadsException{
        Log.d(TAG, "Start writeThreads");
        final ArrayIterator<String> fileNamesIter = new ArrayIterator<>(Constants.FILENAMES);
        final ListIterator<ForumThread> forumThreadsIter = fts.listIterator();
        int exceptions = 0;

        while (fileNamesIter.hasNext() && forumThreadsIter.hasNext()){
            try{
                saveThread(forumThreadsIter.next(), fileNamesIter.next());
            } catch (IOException ignored){
                exceptions++;
            }
        }

        if (exceptions == Math.min(fts.size(), Constants.FILENAMES.length))
            throw new WriteThreadsException("Could not save threads to file");

    }

    private ForumThread readThread(String fileName)
            throws IOException, ClassNotFoundException{
        Log.d(TAG, "Start readThread");

        ForumThread thread;

        FileInputStream fis = openFileInput(fileName);
        ObjectInputStream is = new ObjectInputStream(fis);
        thread = (ForumThread) is.readObject();
        is.close();
        fis.close();
        Log.d(TAG, "Stop readThread");
        return thread;
    }

    private List<ForumThread> readThreads()
            throws ReadThreadsException {
        Log.d(TAG, "Start readThreads");
        final List<ForumThread> forumThreads = new ArrayList<>();
        final ArrayIterator<String> fileNamesIter  = new ArrayIterator<>(Constants.FILENAMES);
        final int fileNums = Constants.FILENAMES.length;
        int exceptions = 0;
        ForumThread forumThread;

        while(fileNamesIter.hasNext()){
            try{
                forumThread = readThread(fileNamesIter.next());
                if (forumThread != null)
                    forumThreads.add(forumThread);
            } catch (IOException | ClassNotFoundException ignored){
                exceptions++;
            }
        }

        if (exceptions == fileNums)
            throw new ReadThreadsException("No files to read");

        Log.d(TAG, "Stop readThreads");
        return forumThreads;
    }

    public static void startUpdateService(Context context){
        setTask(context);
    }

    public static void setUpdateServiceInterval(Context context){
        unSetTask(context);
        setTask(context);
    }

    public static void stopUpdateService(Context context){
        UpdateService.unSetTask(context);
    }

    private static int getIntervalFromPreferences(Context context){
        String key = context.getString(R.string.preference_notifications_interval);
        String preferences = context.getString(R.string.preferences);
        return context.getSharedPreferences(preferences, MODE_PRIVATE).getInt(key, 50);
    }

    private static void setTask(Context context){
        context = context.getApplicationContext();
        int intervalPercentage = getIntervalFromPreferences(context);
        int interval = Utils.getIntervalFromPercentage(intervalPercentage);

        Log.d(TAG, "Setting task with interval : " + interval);

        PeriodicTask periodicTask = new PeriodicTask.Builder()
                .setFlex((long) (Math.ceil((5 * interval) / 100)))
                .setPeriod(interval)
                .setPersisted(true)
                .setRequiredNetwork(Task.NETWORK_STATE_CONNECTED)
                .setRequiresCharging(false)
                .setService(UpdateService.class)
                .setTag("com.shmmy.examresults")
                .setUpdateCurrent(true)
                .build();
        GcmNetworkManager.getInstance(context).schedule(periodicTask);
    }


    private static void unSetTask(Context context){
        context = context.getApplicationContext();
        GcmNetworkManager.getInstance(context).cancelAllTasks(UpdateService.class);
    }


    private class ReadThreadsException extends Exception{

        private ReadThreadsException(String s){
            super(s);
        }

    }

    private class WriteThreadsException extends Exception{

        private WriteThreadsException(String s){
            super(s);
        }

    }

}
