package com.parser.phpbb;

import android.util.Log;
import com.shmmy.examresults.utilities.Constants;
import java.io.Serializable;

/**
 * Created by tilemachos.
 */

public abstract class BulletinBoardEntity implements Serializable{

    private static final String TAG = "BulletinBoardEntity";
    protected String url;
    protected String id;


    @Override
    public boolean equals(Object obj){
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BulletinBoardEntity bbe = (BulletinBoardEntity) obj;
        if (!this.url.equals(bbe.url))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.url != null ? this.url.hashCode() : 0);
        return hash;
    }

    /**
     * Recursively works its way up the hierarchy to build the fully qualified
     * url for this entity.
     * @return  Fully qualified url for this entity.
     */
    public String getFullUrl() {
        return Constants.ROOT_URL + url;
    }

    public String getRootUrl() {
        return Constants.ROOT_URL;
    }

    protected String sanitizeSubUrl( String url){
        Log.d(TAG, "Start sanitize SubUrl");
        url = url.replaceAll("(\\&sid=\\w+)", "");
        if (url.startsWith("http://") || url.startsWith("https://"))
            url = url.replace(Constants.ROOT_URL, "");
        if (url.startsWith("/")) // If subUrl starts with / change it to ./
            url = "." + url;
        if (!url.startsWith("./"))
            url = "./" + url;
        Log.d(TAG, "End sanitize SubUrl");
        return url;
    }

    protected String sanitizeSubUrlWithOffset(String url){
        url = url.replaceAll("(\\&start=\\d+)", "");
        return sanitizeSubUrl(url);
    }

    public String getSubUrl(){
       return url;
    }
}
