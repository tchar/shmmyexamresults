package com.parser.phpbb;

import java.io.Serializable;

/**
 * Created by tilemachos.
 */

public class BulletinBoard extends BulletinBoardEntity implements Serializable{

    public BulletinBoard(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object obj){
        return (super.equals(obj));
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
