package com.parser.phpbb;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by tilemachos.
 */

public class Post extends BulletinBoardEntity implements Serializable {
    private String message;
    private Date date;
    
    public Post(){
        this.url = "";
        date = null;
    }


    public Post(String url) {
        super();
        this.url = url;
        date = null;
    }

    @Override
    public boolean equals(Object obj){
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Post p = (Post) obj;
        if (!this.message.equals(p.message))
            return false;
        if (this.date == null)
            return p.date == null;
        else if (!this.date.equals(p.date))
                return false;

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.message != null ? this.message.hashCode() : 0);
        hash = 53 * hash + (this.date != null ? this.date.hashCode() : 0);
        return hash;
    }

    public void setDate(Date d){
        this.date = d;
    }

    public Date getDate(){
        return this.date;
    }

    public String getMessage(){
    	return this.message;
    }
    
    public void setMessage(String message){
    	this.message = message;
    }
    
    public String getUrl(){
        return this.url;
    }

    public void setUrl(String url){
        this.url = url;
    }

}
