package com.parser.phpbb;

import android.util.Log;

import com.shmmy.examresults.utilities.Constants;
import com.shmmy.examresults.utilities.Utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by tilemachos.
 */

public class PhpBB3Parser {

	private static final String TAG = "PhpBB3Parser";
    private static long lastRequest = 0;


    /**
     * Use this function to connect. Because jsoup needs http:// style links
     * we make sure that every url starts with http:// so jsoup wont hang
     * @param url   url to connect
     * @return      Document Object
     * @throws IOException
     */
    private Document jsoupConnect(String url) throws IOException{
        if (url == null)
            throw new IOException("Url is null are there any other pages left?");
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        Log.d(TAG, "Connecting:" + url);

        long now = System.currentTimeMillis();
        while (now - lastRequest < Constants.MIN_REQUEST_TIME) {
            Log.d(TAG, "Putting thread to sleep");
            try {
                long timeToSleep = Constants.MIN_REQUEST_TIME -
                        (now - lastRequest);
                Thread.sleep(timeToSleep);
                now = System.currentTimeMillis();
            } catch (InterruptedException ignored) {
                Log.d(TAG, "Thread interrupted");
                break;
            } catch (IllegalArgumentException ignored){
                Log.d(TAG, "Put the thread to sleep with negative time");
                break;
            }
        }
        Document doc = Jsoup.connect(url).timeout(3000).userAgent("Mozilla").get();
        lastRequest = System.currentTimeMillis();
        return doc;
    }

    /**
     * Parses each topic for a particular forum.
     * @param  url         Forum the threads belong to
     * @return             List of ForumThread objects
     */
    public List<ForumThread> parseForumThreads(String url) {
        Log.d(TAG, "Start parseForumThreads");

        int threads = 0;
        Forum parent = new Forum(url);
    	List<ForumThread> forumThreads = new ArrayList<>();
    	try{
            Document forum = jsoupConnect(parent.getFullUrl());

            // Get topic table
            Elements threadsTable = forum.select(".bg1,.bg2");
            if (threadsTable.isEmpty()) {
                Log.d(TAG, "It seems " + parent.getFullUrl() + " has no topics.");
                return forumThreads;
            }

            // Loop topics and grab info about each
            for (Element el : threadsTable) {
                if (threads >= Constants.MAX_THREADS)
                    break;
                Elements temp = el.select("a[class=topictitle]");
                if (!Utils.removeAccents(temp.text().toLowerCase())
                        .startsWith(Constants.TOPIC_NAME_STARTS_WITH))
                    continue;

                String newThreadUrl = temp.attr("href");
                String newThreadTitle = temp.text();
                ForumThread newThread = new ForumThread(newThreadUrl);
                newThread.setTitle(newThreadTitle);

                forumThreads.add(newThread);
                threads++;
            }

    	} catch (IOException e) {
            Log.e(TAG, e.getMessage());
    	}
        Log.d(TAG, "End parseForumThreads");
    	return forumThreads;
    }


    /**
     * Gets a String that refers to a date and returns the corresponding Date object.
     * Since the dates inside the forum are custom we must do it manually.
     * @param   s       String that refers to a date
     * @return          Corresponding Date object
     */


    private Date getDateFromString(String s){
        Pattern p = Pattern.compile(Constants.DATE_REGEX_MATCH);
        Matcher m = p.matcher(s);
        Date date = null;

        if (m.find()){
            Locale locale = new Locale("el", "GR");
            String dateStr = m.group();

            SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT, locale);
            DateFormatSymbols dfs = DateFormatSymbols.getInstance(locale);

            dfs.setShortMonths(Constants.MONTHS);
            dfs.setShortWeekdays(Constants.WEEKDAYS);
            dfs.setAmPmStrings(Constants.AM_PM_SYMBOLS);
            df.setDateFormatSymbols(dfs);

            try {
                date = df.parse(dateStr);
            } catch (ParseException e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return date;
    }


    /**
     *  Gets the post body element and returns a Date object according to when
     *  the post was edited/created
     *
     * @param elem      Post body element
     * @return          Date object (when post was edited/created)
     */
    private Date getDate(Element elem){
        Date date;
        // Find post edit date
        Elements elems = elem.select("div[class=postbody] > div > div[class=notice]");
        date = getDateFromString(elems.text());

        // Find post date if post edit never occurred
        if (date == null){
            elems = elem.select("div[class=postbody] > div > p[class=author]");
            date = getDateFromString(elems.text());
        }
        return date;
    }

    /**
     * Gets the postbody element and returns the post link id (a number)
     * @param elem      postbody Element
     * @return          String to be returned
     */
    private String getPostUrl(Element elem){
        Elements elems = elem.select("div[class=postbody] > div > h3 > a");
        if (elems.isEmpty())
            return "";
        elem = elems.get(0);
        return elem.attr("href").replace("#p", "");
    }

    /**
     * Gets an Element object and processes all <a class="postlink"/> and
     * <a class="postimage"/> elements to replace all local links (those starting
     * with "./" with the full forum links.
     * @param elem      An Element object
     */

    private void processLinks(Element elem){
        Elements linkElems = elem.select("a[class=postlink], a[class=postimage]");
        for (Element linkElem : linkElems){
            String replaceLink = linkElem.attr("href");
            if (replaceLink.startsWith("./")){
                replaceLink = replaceLink.replaceAll("&sid=(.)*", "");
                linkElem.attr("href", Constants.ROOT_URL + replaceLink);
            }
        }
    }

    /**
     *  Gets the post content Element and processes all tags that contain "img"
     *  if the img is a smiley then remove that element.
     *  if the img is a postimage then transform it to the equivalent <a href/> style
     * @param contentElem       The post content Element
     */
    private void processImages(Element contentElem){
        Elements elems = contentElem.select("img");
        for (Element elem : elems){
            if (elem.attr("class").equals("smilies"))
                elem.remove();
            else if(elem.attr("class").equals("postimage")){
                String src = elem.attr("src");
                String name = elem.attr("alt");
                if (name.isEmpty())
                    name = Constants.IMG_DEFAULT_NAME;

                Element elem2 = new Element(Tag.valueOf("a"), elem.baseUri());
                elem2.attr("href", src);
                elem2.text(name);
                elem.replaceWith(elem2);
            }
        }
    }

    /**
     * Gets the postcontent element and returns the pure message stripped of unnecessary <div>
     * and <br> elements
     * @param elem      postcontent Element
     * @return          postcontent message
     */
    private String getMessage(Element elem){
        processImages(elem);
        processLinks(elem);

        // Remove unnecessary elements.
        elem.select(".button, .button2, input[type=button]").remove();

        String message = elem.toString();

        // Replace all continuous <br> elements with signle <br>
        message = message.replaceAll("((<\\s*br\\s*>|<\\s*br\\s*/>)\\s*)+", "<br>");
        // Replace all div elements with <br> since they tend to add more lines to the message.
        message = message.replaceAll("<div.*>", "<br>");
        message = message.replaceAll("</(//s)*div>", "");
        // Replace <br> with empty string at start and end of the string message
        message = message.replaceAll("((<br>)+$|^(<br>)+)", "");

        return message;
    }

    private String getAttachments(Element elem){

        Elements attachmentElems = elem.select("div[class=postbody] > div > dl[class=attachbox] > dd > dl");
        if (attachmentElems.isEmpty())
            return "";
        String attachments = "";
        for (Element attachmentElem : attachmentElems){
            processLinks(attachmentElem);
            attachments += attachmentElem.toString() + "<br>";
        }

        // Replace all starting and ending <br>s with empty string
        attachments = attachments.replaceAll("((<br>)+$|^(<br>)+)", "");
        attachments = "<br><br>" + attachments;
        return attachments;
    }


    private String getFullMessage(Element bodyElem, Element contentElem){
        String attachments = getAttachments(bodyElem);
        String message = getMessage(contentElem);

        message += attachments;
        // If there is blockquote followed by newlines remove newlines, since blockquote
        // inserts a newline
        message = message.replaceAll("</\\s*blockquote\\s*>\\s*(<br>)+", "</blockquote>");
        return message;
    }


    /**
     * Parses each post for a particular topic.
     * @param  html         Html containing the posts to be parsed
     * @param threadPage    ThreadPage object
     */
    private void parsePagePosts(Document html, ThreadPage threadPage) {
        Log.d(TAG, "Start parsePagePosts");

        List<Post> posts = new ArrayList<>();
        // Replace each newline in html with a unique string
        // to be replaced with newlines.

        // Find all divs with class content. Each div is a single post
        Elements postsTable = html.select("div[class=postbody]");
        for (Element postBodyElem : postsTable){
            Post newPost = new Post();

            // If there is not any content continue
            Elements postContentElems = postBodyElem.select("div[class=postbody] > div > div[class=content]");
            if (postContentElems.isEmpty())
                continue;

            // Set url of the post
            newPost.setUrl(getPostUrl(postBodyElem));

            // postDate may be null, this doesn't matter
            newPost.setDate(getDate(postBodyElem));

            // Get post content
            Element postContentElem = postContentElems.get(0);

            String message = getFullMessage(postBodyElem, postContentElem);
            newPost.setMessage(message);
            posts.add(newPost);
        }
        threadPage.setPosts(posts);
        Log.d(TAG, "End parsePagePosts");
    }

    /**
     *  Gets the root element and the current page and gets the true current page from
     *  the root element. If the page found from root and the current page match
     *  return true, else return false
     *
     *
     * @param root          root element of the page
     * @param currentPage   current page
     * @return              returns true if currentPage is correct based on root element
     */
    private boolean checkPage(Element root, int currentPage){
        boolean pageCorrect = true;
        // If pages are more than 1 then this gets the current page
        Elements currentPageElems = root.select("div[class=pagination] > ul > li[class=active]");

        if (!currentPageElems.isEmpty()){
            Element currentPageElem = currentPageElems.get(0);
            try{
                int testPage = Integer.parseInt(currentPageElem.text().trim());
                // If we are not on a correct page set pageCorrect to false
                if (testPage - 1 != currentPage){
                    pageCorrect = false;
                }
            } catch (NumberFormatException ignored){
            }
        }
        // If page is 1 then else is triggered
        else {
            // If we are not on a correct page set pageCorrect to false
            if (currentPage != 0)
                pageCorrect = false;
        }
        return pageCorrect;
    }

    /**
     * Parses each thread for all posts.
     * @param forumThreads  forumThreads
     */
    public void parsePosts(List<ForumThread> forumThreads) {
        //TODO Fix first only post in page gets deleted, page redirects to last page and have a new copy of last same page.
        Log.d(TAG, "Start parsePosts");
    	for (ForumThread forumThread : forumThreads){
            int pageCounter = 0;
            Document lastRoot = null;
            boolean connection = true;
            int firstPage = forumThread.getPages() - Constants.LAST_THREAD_PAGES_REFRESH;
            if (firstPage < 0)
                firstPage = 0;
            forumThread.removeLastPages(firstPage + 1);
            while(connection && pageCounter < Constants.MAX_PAGES){
                try{
                    int currentPage = firstPage + pageCounter;
                    if (currentPage < 0)
                        currentPage = 0;

                    Log.d(TAG, "Forum:" + forumThread.getTitle());
                    Log.d(TAG, "Page:" + (currentPage + 1));
                    Log.d(TAG, "" + forumThread.getPageFullUrl(currentPage));
                    Document root = jsoupConnect(forumThread.getPageFullUrl(currentPage));
                    if (root.equals(lastRoot))
                        break;

                    // Check if page is correct

                    if (!checkPage(root, currentPage)){
                        Log.d(TAG, "Wrong page detected");
                        firstPage--;
                        forumThread.removeLastPages(firstPage + 1);
                        continue;
                    }

                    // Get the next page if exists
                    Elements nextPageElems = root.select("div[class=pagination] > ul > li[class=next] > a");

                    if (!nextPageElems.isEmpty()){
                        String nextUrl = nextPageElems.get(0).attr("href");
                        forumThread.addPage(nextUrl);
                    } else{
                        Log.d(TAG, "No more pages found");
                        connection = false;
                    }

                    // Parse posts
                    parsePagePosts(root, forumThread.getThreadPage(currentPage));
                    pageCounter++;
                    lastRoot = root;
                } catch (IOException e){
                    Log.e(TAG, e.getMessage());
                    connection = false;
                }
            }
        }

        Log.d(TAG, "End parsePosts");
    	
    }

   
}
