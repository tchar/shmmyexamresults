package com.parser.phpbb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tilemachos on 7/8/15.
 */

public class ThreadPage extends BulletinBoardEntity implements Serializable{

    private List<Post> posts;

    public ThreadPage(String url, int mode){
        if (mode == ForumThread.MODE_NEW_THREAD)
            this.url = this.sanitizeSubUrlWithOffset(url);
        else if (mode == ForumThread.MODE_NEW_PAGE)
            this.url = this.sanitizeSubUrl(url);
        posts = new ArrayList<>();
    }

    public List<Post> getPosts(){
        return posts;
    }

    @Override
    public boolean equals(Object obj){
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ThreadPage tp = (ThreadPage) obj;
        if (!this.posts.equals(tp.posts))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.posts != null ? this.posts.hashCode() : 0);
        return hash;
    }

    public void setPosts(List<Post> posts){
        this.posts = posts;
    }

}
