
package com.parser.phpbb;

import com.shmmy.examresults.utilities.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tilemachos.
 */

public class ForumThread implements Serializable{

    final static int MODE_NEW_THREAD = 0;
    final static int MODE_NEW_PAGE = 1;
    private final List<ThreadPage> threadPages;
    private String title;

    public ForumThread(String url) {
        threadPages = new ArrayList<>();
        threadPages.add(new ThreadPage(url, MODE_NEW_THREAD));
    }


    @Override
    public boolean equals(Object obj){
        if (getClass() != obj.getClass())
            return false;
        ForumThread ft = (ForumThread) obj;
        if (!this.threadPages.equals(ft.threadPages))
            return false;
        if (!this.title.equals(ft.title))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.threadPages != null ? this.threadPages.hashCode() : 0);
        hash = 53 * hash + (this.title != null ? this.title.hashCode() : 0);
        return hash;
    }

    public ThreadPage getThreadPage(int page){
        return threadPages.get(page);
    }

    public List<Post> getPagePosts(int page){
        return threadPages.get(page).getPosts();
    }

    public List<Post> getAllPosts(){
        List<Post> posts = new ArrayList<>();
        for (ThreadPage threadPage : threadPages){
            posts.addAll(threadPage.getPosts());
        }
        return posts;
    }

    public int getPages(){
        return threadPages.size();
    }

    public String getThreadSubUrl(){
        if (threadPages.size() == 0)
            return "";
        return threadPages.get(0).getSubUrl();
    }

    public String getThreadFullUrl(){
        return Constants.ROOT_URL + getThreadSubUrl();
    }

    public void addPage(String url){
        threadPages.add(new ThreadPage(url, MODE_NEW_PAGE));
    }

    public void removeLastPages(int from){
        int size = getPages();
        try {
            threadPages.subList(from, size).clear();
        } catch (IndexOutOfBoundsException ignored){
        }
    }

    public String getPageFullUrl(int page){
        if (page >= threadPages.size())
            return null;
        return threadPages.get(page).getFullUrl();
    }

    public String getPageSubUrl(int page){
        if (page >= threadPages.size())
            return null;
        return threadPages.get(page).getSubUrl();
    }

    public List<ThreadPage> getThreadPages() {
        return threadPages;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return title;
    }
}
