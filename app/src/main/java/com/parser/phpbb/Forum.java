/**
 * Created by tilemachos.
 */


package com.parser.phpbb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Forum extends BulletinBoardEntity implements Serializable{
    private int numTopics;
    private final List<ForumThread> topics;

    public Forum(String url) {
        super();
        numTopics = 0;
        topics = new ArrayList<>();
        this.url = this.sanitizeSubUrlWithOffset(url);
    }

    @Override
    public boolean equals(Object obj){
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Forum f = (Forum) obj;
        return (this.topics.equals(f.topics));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.topics != null ? this.topics.hashCode() : 0);
        return hash;
    }

    public List<ForumThread> getThreads(){
        return topics;
    }

    public int getNumThreads(){
        return numTopics;
    }

    public void addThread(ForumThread forumThread){
        topics.add(forumThread);
    }
}
