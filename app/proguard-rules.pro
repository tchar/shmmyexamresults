# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/tilemachos/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-dontpreverify
-repackageclasses ''
-allowaccessmodification
-optimizations !code/simplification/arithmetic
-keepattributes *Annotation*

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider

-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class * implements android.os.Parcelable {
    static android.os.Parcelable$Creator CREATOR;
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

-keep public class * extends android.support.v4.app.Fragment
-keep public class * extends android.app.Fragment

-keepnames class * implements java.io.Serializable

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    !static !transient <fields>;
    !private <fields>;
    !private <methods>;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

-keepclasseswithmembernames class * {
    native <methods>;
}
-keepclassmembers class * {
    public void *ButtonClicked(android.view.View);
}

## Rules for phpbb parser
#-keep public class com.parser.phpbb.** { *; }
#-keep class com.parser.phpbb.** { *; }
#-keep interface com.parser.phpbb.** { *; }
#
## Rules for appcompat
#-keep public class android.support.v7.** { *; }
#-keep class android.support.v7.** { *; }
#-keep interface android.support.v7.** { *; }
#-keep public class android.support.v4.** { *; }
#-keep class android.support.v4.** { *; }
#-keep interface android.support.v4.** { *; }
#
## Rules for support design
#-keep class android.support.design.** { *; }
#-keep interface android.support.design.** { *; }
#-keep public class android.support.design.** { *; }
#
## Rules for jsoup
#-keep public class org.jsoup.** { *; }
#-keep class org.jsoup.** { *; }
#-keep interface org.jsoup.** { *; }
#
## Rules for google gms
#-keep public class com.google.android.gms.gcm.** { *; }
#-keep class com.google.android.gms.gcm.** { *; }
#-keep interface com.google.android.gms.gcm.** { *; }
#
## Rules for joda (time)
-dontwarn org.joda.convert.**
#-keep public class org.joda.** { *; }
#-keep class org.joda.** { *; }
#-keep interface org.joda.** { *; }
#
## Rules for aniqroid
-dontwarn com.sileria.android.**
#-keep public class com.sileria.android.** { *; }
#-keep class com.sileria.android.** { *; }
#-keep interface com.sileria.android.** { *; }


# Remove logs. Only when using 'proguard-android-optimize.txt'
-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String, int);
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);
}
